# javascript's impicit coerion đề cập đến  vấn đề  convert data  từ  dạng này sang dạng khác  cho phù hợp  vơi quy tắc

* source : https://dev.to/promhize/what-you-need-to-know-about-javascripts-implicit-coercion-e23 

=> xoắn quẩy vc
```js
3 * "3" //9
1 + "2" + 1 //121

true + true //2
10 - true //9


const foo = {
  valueOf: () => 2
}
3 + foo // 5
4 * foo // 8

const bar = {
  toString: () => " promise is a boy :)"
}
1 + bar // "1 promise is a boy :)"


4 * [] // 0
4 * [2] // 8
4 + [2] // "42"
4 + [1, 2] // "41,2"
4 * [1, 2] // NaN

"string" ? 4 : 1 // 4
undefined ? 4 : 1 // 1

```

## biểu thức toán vs các giá trị không phải là số 
### 1. String :
* khi thự hiện các phép toán với string  (- * / %) => thực hiện chuyển đổi  string thành số  = `NUmber (String)`
* nếu có ký tự string khong bieuu thị so thì chuyển về Na
* `phép cộng ` thì ngược lại : chuyển  number to string 

### 2. Object 
* mỗi object điều kế thừa toString method  và được gọi khi object  convert sang string 
* mặc định là "[object Object]"

```js
const foo = {}
foo.toString() // [object Object]

const baz = {
  toString: () => "I'm object baz"
}

baz + "!" // "I'm object baz!"
```

### 3.  Array object 
* hàm toString() của array hoạt động tương tự như join()=> dược  gọi khi convert 

```js
[1,2,3].toString() // "1,2,3"
[1,2,3].join() // "1,2,3"
[].toString() // ""
[].join() // ""

"me" + [1,2,3] // "me1,2,3"
4 + [1,2,3] // "41,2,3"
4 * [1,2,3] // NaN
``` 

* kh expects number thì convert number khi expect string thì convert string ...


```js
4 * [] // 0
4 / [2] // 2

//similar to
4 * Number([].toString())
4 * Number("")
4 * 0

//

4 / Number([2].toString())
4 / Number("2")
4 / 2
```
### True false and ""
* true => 1 if expects NUmber and return 'true' if expects string 
* false => 0
* "" => 0


#### valueOf and toString method =>> ??
* khi object có 2 hàm thì nó chuyển sử dụng hàm valueOf 

```js
const bar = {
  toString: () => 2,
  valueOf: () => 5
}

"sa" + bar // "sa5"
3 * bar // 15
2 + bar // 7

const two = new Number(2)

two.valueOf() // 2

```
### Falsy and Truthy
* in js các giá trị sẽ convert về false là :
1. false
2. 0
3. null
4. undefined
5. ""
6. NaN
7. -0

* everythong else í truthly 



### NaN +>> NaN === NaN // false 

```js
// let a = {};
// let b = [1,2,1,2,3];
// b.forEach(e=>a[e] = -~a[e])
// console.log(a)
// return inputArray.map(e=>{
//     let s = new Set();
//     [...e].forEach(i => s.add(i) );
//     return Array.from(s).join('')
// })




function stringsRearrangement(inputArray) {
    let {length: l} = inputArray, result = []
    function backTrack(k) {
        let D = dnd(result,inputArray);
        for(let i=0; i< D.length; i++) {
            if(diffOne(result[result.length-1],D[i])){
                result.push(D[i])
                if(result.length = l) return result;
                else{
                    backTrack(k+1);
                    result.slice(-1);
                }
            }
        }
    }
}

function dnd(re, src) {
    re.map(e=>src.splice(src.indexOf(e),1))
    return src;
}

function diffOne(a, b) {
    let x = 0;
    [...a].forEach((e, i)=>{ if(a[i]===b[i]) x++;})
    return x===(a.length-1)
}

const data = ["ab", "bb", "aa"]
// console.log(stringsRearrangement(data))
// console.log(diffOne('ab', 'aa'))
console.log(dnd(['ab'], ['aa','ab']))




```
