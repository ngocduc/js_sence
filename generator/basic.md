# run..stop...run
* es6 generrator là 1 loại function mà ta có thể  paused lại, kiểm  soat việc nó  chạy  hay dừng lại 
* `cooperate code` contrasted with "preemptive" 
* nó paused by itself `yield` nhưng chỉ có thể tiếp tục  nhờ `next`
* messages out with each yield
* send messages back in with each restart.
# syntax 
```js
function *foo() {
    // ..
}
```
* when we restart the generator, we will send a value back in,
```js
function *foo() {
    var x = 1 + (yield "foo");
    console.log(x);
}
```

## For completeness sake
* khả năng tương tác

```js
function *foo(x) {
    var y = 2 * (yield (x + 1));
    var z = yield (y / 3);
    return (x + y + z);
}

var it = foo( 5 );

// note: not sending anything into `next()` here
console.log( it.next() );       // { value:6, done:false }
console.log( it.next( 12 ) );   // { value:8, done:false } 12 * 2 / 3 ?? 
console.log( it.next( 13 ) );   // { value:42, done:true } 
```
* yield thứ 2 vơi tham số đầu vào là giá trị của  yeild  liền trc  nó \

```js
// note: `foo(..)` here is NOT a generator!!
function foo(x) {
    console.log("x: " + x);
}

function *bar() {
    yield; // just pause
    foo( yield ); // pause waiting for a parameter to pass into `foo(..)`
}
```
# for .. of 
```js
function *foo() {
    yield 1;
    yield 2;
    yield 3;
    yield 4;
    yield 5;
    return 6;
}

for (var v of foo()) {
    console.log( v );
}
// 1 2 3 4 5

console.log( v ); // still `5`, not `6` :(
```
for of lấy đc giá trị của các lần yeild mỗi lần như vậy  trã về giá trị ...